package com.ostrowski.healthpointedu;

import android.content.Intent;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Date;

public class resumetriage extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_resumetriage );
        Database.setDataOfStartAction( new Date(  ) );
        Toast.makeText(this, "Akcja zakończona !!! Pobieranie statystyk",
                Toast.LENGTH_LONG).show();

        TextView avpu = findViewById( R.id.avpu );
        TextView czas = findViewById( R.id.czas );
        TextView zag2 = findViewById( R.id.zagrozenia );


        Date t1 = Database.getDataofStopAction();
        Date t2 = Database.getDataOfStartAction();
        long diff = t1.getTime() - t2.getTime();
        long diffSeconds = (diff / 1000) % 60;
        String sec, min, hrs;

        if (diffSeconds < 10) {
            sec = "0" + Long.toString( diffSeconds );
        } else {
            sec = Long.toString( diffSeconds );
        }

        long diffMinutes = (diff / (60 * 1000)) % 60;

        if (diffMinutes < 10) {
            min = "0" + Long.toString( diffMinutes );
        } else {
            min = Long.toString( diffMinutes );
        }

        long diffHours = diff / (60 * 60 * 1000);

        if (diffHours < 10) {
            hrs = "0" + Long.toString( diffHours );
        } else {
            hrs = Long.toString( diffHours );
        }
        String durationText= hrs + ":" + min + ":" + sec;

        czas.setText( durationText );

        int zag=0;

        for(int i =0; i<Database.instrukcja.length;i++){
            if(Database.instrukcja[i]==1)
                zag++;
        }

        zag2.setText( Integer.toString( zag ) );
    }

    public void gotoMenu(View view) {
        Intent intent = new Intent( this, Menu.class );
        startActivity( intent );
    }
}
