package com.ostrowski.healthpointedu;

import android.provider.ContactsContract;

import java.util.Date;

public class Database {
    static public Date dataOfStartAction=null;
    static public Date dataofStopAction=null;
    static public int push_counter=0;
    static public int cycle=0;
    static public int breath_counter=0;
    static public int heart=0;
    static public int instrukcja[] = new int[10];
    static public int instrukcja_strona=0;
    static public int avpu=0;
    static public String instrukcje_text[]=new String[10];


    public static void update(){
        instrukcje_text[0]="Are the airways open (a wheezing and wheezing noise at\n" +
                "inhalation, indicates partial obstruction of the upper respiratory tract 135)\n" +
                "- If so, place the injured person in a side position\n" +
                "- In the event of complete obstruction (no sound, no breathing while conscious) you should stand behind the victim slightly to the side, put your left hand under his armpit and put your hand on your chest, leaning the person forward, the other hand hits the back vigorously between the shoulder blades . We make five strokes";

        instrukcje_text[1]=  "Exclude unidentified sources of bleeding. For life-threatening external hemorrhage, put a tourniquet 5-7.5 cm above the wound.";

        instrukcje_text[2]=  "Broken leg should be immobilized in a simple and gentle way, remembering about the threat of shock. For this purpose, two adjacent joints are immobilized. In the case of the lower limb, it can be immobilized by binding to the other leg, bypassing the fracture area, using a makeshift splint, e.g. from folded newspaper or cardboard.";



        instrukcje_text[3]=
                "First aid for unconsciousness begins with a safety check. In the event of danger, refrain from providing assistance. The next step is to assess the patient's condition, including response to voice commands. Next, you need to notify the ambulance as soon as possible. The next action is to clear the airways, without unnecessary movement of the victim's head due to possible injury to the cervical spine. If a person is breathing properly and there are no convulsions or cardiac arrest, his legs can be raised. This significantly improves blood flow to the heart.";


        instrukcje_text[4]=  "If this is not necessary, do not remove your outer clothing or change to wet clothing. A wounded person should be covered with a Ready-Heat Blanket with HPMK 139 (Hypothermia Prevention And Managment Kit)";

        instrukcje_text[5]=  "A quick eye examination of the casualty should be performed, then rinse the eye and cover the eye with a dressing";

        instrukcje_text[6]=  "If there is a burn of the face, it may also mean burns to the roads\n" +
                " breathing, observe breathing and give additional oxygen\n" +
                " - estimate the degree of body burn\n" +
                " - burns with a dry dressing;\n" +
                "\n";

    }
    public static int getHeart() {
        return heart;
    }

    public static void setHeart(int heart) {
        Database.heart = heart;
    }

    public static int getCycle() {
        return cycle;
    }

    public static void setCycle(int cycle) {
        Database.cycle = cycle;
    }

    public static Date getDataofStopAction() {
        return dataofStopAction;
    }

    public static void setDataofStopAction(Date dataofStopAction) {
        Database.dataofStopAction = dataofStopAction;
    }

    public static int getPush_counter() {
        return push_counter;
    }

    public static void setPush_counter(int push_counter) {
        Database.push_counter = push_counter;
    }

    public static int getBreath_counter() {
        return breath_counter;
    }

    public static void setBreath_counter(int breath_counter) {
        Database.breath_counter = breath_counter;
    }



    public static Date getDataOfStartAction() {
        return dataOfStartAction;
    }

    public static void setDataOfStartAction(Date dataOfStartAction) {
        Database.dataOfStartAction = dataOfStartAction;
    }
}
