package com.ostrowski.healthpointedu;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import java.util.Date;

public class triage_instruction extends AppCompatActivity {
    TextView text;
    TextView pulsik;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_triage_instruction );
        text = findViewById( R.id.text );

        pulsik=findViewById( R.id.pulse );
        pulsik.setText( Integer.toString( Database.getHeart() ) );
        int flaga=0;

        while (flaga==0){
            if(Database.instrukcja[Database.instrukcja_strona]==0){
                Database.instrukcja_strona++;
            }else
                flaga=1;

            if(Database.instrukcja_strona==6){
                end();
            }

        }
        Database.update();
        text.setText( Database.instrukcje_text[Database.instrukcja_strona] );
    }

    public void next(View view) {
        Database.instrukcja_strona++;


        int flaga=0;

        while (flaga==0){
            if(Database.instrukcja[Database.instrukcja_strona]==0){
                Database.instrukcja_strona++;
            }else
                flaga=1;

            if(Database.instrukcja_strona==6){
                flaga=1;
                Database.setDataofStopAction( new Date(  ) );
                Intent intent = new Intent( triage_instruction.this, resumetriage.class );
                startActivity( intent );
            }

        }
        Database.update();
        text.setText( Database.instrukcje_text[Database.instrukcja_strona] );
    }

    private void end() {
        Database.setDataofStopAction( new Date(  ) );
        Intent intent = new Intent( triage_instruction.this, resumetriage.class );
        startActivity( intent );
    }

    public void stop(View view) {
      end();
    }
}
