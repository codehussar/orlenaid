package com.ostrowski.healthpointedu;

import android.Manifest;
import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.graphics.Camera;
import android.graphics.Matrix;
import android.graphics.RectF;
import android.graphics.SurfaceTexture;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCaptureSession;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraDevice;
import android.hardware.camera2.CameraManager;
import android.hardware.camera2.CaptureRequest;
import android.hardware.camera2.params.StreamConfigurationMap;
import android.location.GpsSatellite;
import android.location.GpsStatus;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.location.LocationProvider;
import android.media.MediaPlayer;
import android.opengl.Visibility;
import android.os.AsyncTask;
import android.os.BatteryManager;
import android.os.Build;
import android.os.Handler;
import android.os.HandlerThread;
import android.provider.ContactsContract;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresPermission;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.CellInfoGsm;
import android.telephony.CellSignalStrengthGsm;
import android.telephony.SignalStrength;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.util.Size;
import android.util.SparseIntArray;
import android.view.OrientationEventListener;
import android.view.Surface;
import android.view.TextureView;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMapOptions;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnSuccessListener;
import com.jjoe64.graphview.DefaultLabelFormatter;
import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.GridLabelRenderer;
import com.jjoe64.graphview.Viewport;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;
import com.mapbox.mapboxsdk.Mapbox;
import com.mapbox.mapboxsdk.constants.Style;
import com.mapbox.mapboxsdk.maps.MapView;
import com.microsoft.band.BandClient;
import com.microsoft.band.BandClientManager;
import com.microsoft.band.BandException;
import com.microsoft.band.BandInfo;
import com.microsoft.band.ConnectionState;
import com.microsoft.band.UserConsent;
import com.microsoft.band.sensors.BandHeartRateEvent;
import com.microsoft.band.sensors.BandHeartRateEventListener;
import com.microsoft.band.sensors.HeartRateConsentListener;


import java.lang.ref.WeakReference;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import javax.xml.datatype.Duration;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static java.lang.Thread.sleep;


public class MainActivity extends AppCompatActivity implements SensorEventListener, OnMapReadyCallback {

    private TextureView textureView;
    private static final SparseIntArray ORINTATION = new SparseIntArray();

    static {
        ORINTATION.append( Surface.ROTATION_0, 90 );
        ORINTATION.append( Surface.ROTATION_90, 0 );
        ORINTATION.append( Surface.ROTATION_180, 270 );
        ORINTATION.append( Surface.ROTATION_270, 180 );
    }

    private String cameraid;
    private CameraDevice cameraDevice;
    private CameraCaptureSession cameraCaptureSession;
    private Size imageDimension;
    private int REQUEST_CAMERA_PERMISSION = 200;
    private Handler mBackgroundHandler;
    private HandlerThread mBackgroundThread;
    private CameraCaptureSession cameraCaptureSessions;
    private CaptureRequest.Builder captureRequestBuilder;
    private LocationManager locationManager;
    private LocationListener locationListener;
    private Handler handler = new Handler();
    private TextView action_text;
    private Animation animation;
    private Animation animation_out;
    private Context context = this;
    private TextView fromAccident;
    private TextView actualTime;
    private TextView data;
    private Date startTime;
    private TextView battery;
    private ImageView aim;
    private SensorManager sensorManager;
    private Sensor sensor;
    private int count;
    private TextView count_to;
    private TextView count_down;
    private ProgressBar alarm;
    private GraphView graph;
    private LineGraphSeries<DataPoint> series = new LineGraphSeries<DataPoint>();
    private TextView longLat;
    private TextView lat;
    private TextView signal;
    private LocationListener listener;
    private MapView mapView;
    private GoogleMap googleMap;
    private GoogleMap gmap;
    private SupportMapFragment mapFragment;
    private GoogleMap map;
    private List<Float> dataAccelerometr = new ArrayList<Float>();
    private TextView azymut;
    private TextView roll_t;
    float Rot[]=null; //for gravity rotational data
    //don't use R because android uses that for other stuff
    float I[]=null; //for magnetic rotational data
    float accels[]=new float[3];
    float mags[]=new float[3];
    float[] values = new float[3];

    float azimuth;
    float pitch;
    float roll;
    private int mapa=0;

    private BandClient client = null;
    private TextView heart;
    private ImageView heart_image;
    private ImageView breath_icon;
    private TextView breath;


    private static final String MAP_VIEW_BUNDLE_KEY = "MapViewBundleKey";


    CameraDevice.StateCallback stateCallback = new CameraDevice.StateCallback() {

        @Override
        public void onOpened(@NonNull CameraDevice camera) {
            cameraDevice = camera;
            createCameraPreview();
        }

        @Override
        public void onDisconnected(@NonNull CameraDevice camera) {
            cameraDevice.close();
        }

        @Override
        public void onError(@NonNull CameraDevice camera, int error) {
            cameraDevice.close();
            cameraDevice = null;
        }
    };



    private void transformimage(int width, int height) {

        if (textureView == null) {

            return;
        } else try {
            {
                Matrix matrix = new Matrix();
                int rotation = getWindowManager().getDefaultDisplay().getRotation();
                RectF textureRectF = new RectF( 0, 0, width, height );
                RectF previewRectF = new RectF( 0, 0, textureView.getHeight(), textureView.getWidth() );
                float centerX = textureRectF.centerX();
                float centerY = textureRectF.centerY();
                if (rotation == Surface.ROTATION_90 || rotation == Surface.ROTATION_270) {
                    previewRectF.offset( centerX - previewRectF.centerX(), centerY - previewRectF.centerY() );
                    matrix.setRectToRect( textureRectF, previewRectF, Matrix.ScaleToFit.FILL );
                    float scale = Math.max( (float) width / width, (float) height / width );
                    matrix.postScale( scale, scale, centerX, centerY );
                    matrix.postRotate( 90 * (rotation - 2), centerX, centerY );
                }
                textureView.setTransform( matrix );
            }
        } catch (Exception e) {
            e.printStackTrace();


        }
    }

    public static void ImageViewAnimatedChange(Context c, final ImageView v, final int new_image) {
        final Animation anim_out = AnimationUtils.loadAnimation( c, android.R.anim.fade_out );
        final Animation anim_in = AnimationUtils.loadAnimation( c, android.R.anim.fade_in );
        anim_out.setAnimationListener( new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                v.setImageResource( new_image );
                anim_in.setAnimationListener( new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {
                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                    }
                } );
                v.startAnimation( anim_in );
            }
        } );
        v.startAnimation( anim_out );
    }


    private void updatePreview() {
        if (cameraDevice == null)
            Toast.makeText( this, "Error", Toast.LENGTH_SHORT ).show();
        captureRequestBuilder.set( CaptureRequest.CONTROL_MODE, CaptureRequest.CONTROL_MODE_AUTO );
        try {
            cameraCaptureSessions.setRepeatingRequest( captureRequestBuilder.build(), null, mBackgroundHandler );
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }

    private void createCameraPreview() {
        try {
            SurfaceTexture texture = textureView.getSurfaceTexture();
            assert texture != null;
            texture.setDefaultBufferSize( imageDimension.getWidth(), imageDimension.getHeight() );
            Surface surface = new Surface( texture );
            captureRequestBuilder = cameraDevice.createCaptureRequest( CameraDevice.TEMPLATE_PREVIEW );
            captureRequestBuilder.addTarget( surface );
            cameraDevice.createCaptureSession( Arrays.asList( surface ), new CameraCaptureSession.StateCallback() {
                @Override
                public void onConfigured(@NonNull CameraCaptureSession cameraCaptureSession) {
                    if (cameraDevice == null)
                        return;
                    cameraCaptureSessions = cameraCaptureSession;
                    updatePreview();
                }

                @Override
                public void onConfigureFailed(@NonNull CameraCaptureSession cameraCaptureSession) {
                    Toast.makeText( MainActivity.this, "Changed", Toast.LENGTH_SHORT ).show();
                }
            }, null );
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }

    public static int getBatteryPercentage(Context context) {

        IntentFilter iFilter = new IntentFilter( Intent.ACTION_BATTERY_CHANGED );
        Intent batteryStatus = context.registerReceiver( null, iFilter );

        int level = batteryStatus != null ? batteryStatus.getIntExtra( BatteryManager.EXTRA_LEVEL, -1 ) : -1;
        int scale = batteryStatus != null ? batteryStatus.getIntExtra( BatteryManager.EXTRA_SCALE, -1 ) : -1;

        float batteryPct = level / (float) scale;

        return (int) (batteryPct * 100);
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    public void manager() {
        Runnable start_clock = new Runnable() {
            @Override
            public void run() {
                while (true)
                    set();
            }
        };
        new Thread( start_clock ).start();
    }

    private void set() {
        try {
            sleep( 1000 );

        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        handler.post( new Runnable() {
            @Override
            public void run() {
                SimpleDateFormat sdf = new SimpleDateFormat( "HH:mm:ss" );
                String currentDateandTime = sdf.format( new Date() );
                actualTime.setText( currentDateandTime );
                Date t1 = new Date();
                Database.setDataOfStartAction( startTime );
                Date t2 = Database.getDataOfStartAction();

                long diff = t1.getTime() - t2.getTime();
                long diffSeconds = (diff / 1000) % 60;
                String sec, min, hrs;

                if (diffSeconds < 10) {
                    sec = "0" + Long.toString( diffSeconds );
                } else {
                    sec = Long.toString( diffSeconds );
                }

                long diffMinutes = (diff / (60 * 1000)) % 60;

                if (diffMinutes < 10) {
                    min = "0" + Long.toString( diffMinutes );
                } else {
                    min = Long.toString( diffMinutes );
                }

                long diffHours = diff / (60 * 60 * 1000);

                if (diffHours < 10) {
                    hrs = "0" + Long.toString( diffHours );
                } else {
                    hrs = Long.toString( diffHours );
                }
                fromAccident.setText( hrs + ":" + min + ":" + sec );

                battery.setText( Integer.toString( (int) getBatteryPercentage( context ) ) + "%" );

                addEntry();

            }
        } );
    }


    private BandHeartRateEventListener mHeartRateEventListener = new BandHeartRateEventListener() {
        @Override
        public void onBandHeartRateChanged(final BandHeartRateEvent event) {
            if (event != null) {
                Database.setHeart( event.getHeartRate() );
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_main );

        Toast.makeText(this, "Rozpoczęto akcję ratunkową",
                Toast.LENGTH_LONG).show();

        Database.setDataOfStartAction( new Date(  ) );
        heart = findViewById( R.id.hert_number );
        heart_image = findViewById( R.id.heart );
        animation = AnimationUtils.loadAnimation( this, R.anim.splash_screen );
        animation_out = AnimationUtils.loadAnimation( this, R.anim.splash_screen_out );

        startTime = Calendar.getInstance().getTime();
        battery = findViewById( R.id.battery );
        data = findViewById( R.id.data );
        actualTime = findViewById( R.id.actualTime );
        fromAccident = findViewById( R.id.fromAccidentTime );
        aim = findViewById( R.id.aim );
        count_down = findViewById( R.id.count_down );
        count_to = findViewById( R.id.count_to );
        azymut = findViewById( R.id.azymut );
        roll_t = findViewById( R.id.roll );


        sensorManager = (SensorManager) getSystemService( SENSOR_SERVICE );
        sensorManager.registerListener( this, sensorManager.getDefaultSensor( Sensor.TYPE_ACCELEROMETER ), SensorManager.SENSOR_DELAY_NORMAL );
        sensorManager.registerListener(this, sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD),SensorManager.SENSOR_DELAY_NORMAL);

        SimpleDateFormat sdf = new SimpleDateFormat( "yyyy.MM.dd" );
        String currentDateandTime = sdf.format( new Date() );
        //String dayOfWeek = new SimpleDateFormat("EEEE", Locale.ENGLISH).format(new Date(  ));
        data.setText( currentDateandTime );

        graph = (GraphView) findViewById( R.id.chart );
        addEntry();
        graph.addSeries( series );

        Viewport viewport = graph.getViewport();
        viewport.setXAxisBoundsManual( true );
        viewport.setMinX( 0 );
        viewport.setMaxX( 10 );
        viewport.setYAxisBoundsManual( true );
        viewport.setMaxY( 120 );
        viewport.setMinY( 0 );

        final WeakReference<Activity> reference = new WeakReference<Activity>(this);
        new HeartRateSubscriptionTask().execute();
        new HeartRateConsentTask().execute(reference);

        lat = findViewById( R.id.Latitude );
        longLat = findViewById( R.id.Longtitude );

        graph.getGridLabelRenderer().setLabelFormatter( new DefaultLabelFormatter() {
            @Override
            public String formatLabel(double value, boolean isValueX) {
                if (isValueX) {
                    // show normal x values
                    //  return super.formatLabel(value, isValueX);
                    return "";
                } else {
                    // show currency for y values
                    return "";// super.formatLabel(value, isValueX) + " €";
                }
            }
        } );


        if (ActivityCompat.checkSelfPermission( this, Manifest.permission.ACCESS_FINE_LOCATION ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission( this, Manifest.permission.ACCESS_COARSE_LOCATION ) != PackageManager.PERMISSION_GRANTED) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions( new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION}
                        , 10 );
            }
            return;
        }

        if (ActivityCompat.checkSelfPermission( this, Manifest.permission.INTERNET ) != PackageManager.PERMISSION_GRANTED) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions( new String[]{Manifest.permission.INTERNET, Manifest.permission.ACCESS_NETWORK_STATE}
                        , 10 );
            }
            return;
        }
        locationManager = (LocationManager) getSystemService( LOCATION_SERVICE );

        listener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                NumberFormat formatter = new DecimalFormat( "#0.00" );
                lat.setText( formatter.format( location.getLatitude() ) );
                longLat.setText( formatter.format( location.getLongitude() ) );

                int satellites = 0;
                int satellitesInFix = 0;
                if (ActivityCompat.checkSelfPermission( context, Manifest.permission.ACCESS_FINE_LOCATION ) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return;
                }


            }

            @Override
            public void onStatusChanged(String s, int i, Bundle bundle) {

            }

            @Override
            public void onProviderEnabled(String s) {

            }

            @Override
            public void onProviderDisabled(String s) {

                Intent i = new Intent( Settings.ACTION_LOCATION_SOURCE_SETTINGS );
                startActivity( i );
            }
        };

        NumberFormat formatter = new DecimalFormat( "#0.00" );
        locationManager.requestLocationUpdates( "gps", 5000, 0, listener );
        Location location = locationManager.getLastKnownLocation( LocationManager.GPS_PROVIDER );

      //  TelephonyManager telephonyManager = (TelephonyManager) getSystemService( Context.TELEPHONY_SERVICE );

       // CellInfoGsm GSM = (CellInfoGsm) telephonyManager.getAllCellInfo().get( 0 );
       // CellSignalStrengthGsm cellSignalStrengthGsm = GSM.getCellSignalStrength();
      //  int signalStrength = cellSignalStrengthGsm.getDbm();
        signal = findViewById( R.id.signal );
     //   signal.setText( Integer.toString( signalStrength ) + "dB" );

       SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
               .findFragmentById( R.id.mapa );
        mapFragment.getMapAsync( this );

        breath_icon = findViewById( R.id.breath );
        breath=findViewById( R.id.breath_counter );
        manager();
    }

    Random random = new Random( Integer.MAX_VALUE );

    private void addEntry() {
        int pulseVal = random.nextInt(2)+70;

        series.appendData( new DataPoint( series.getHighestValueX() + 1, pulseVal ) , true, 10 );
        Log.d("e", Integer.toString( random.nextInt(2)+70 ));

        heart.setText(Integer.toString( pulseVal));

        ObjectAnimator scaleDown = ObjectAnimator.ofPropertyValuesHolder(
                heart_image,
                PropertyValuesHolder.ofFloat("scaleX", 1.1f),
                PropertyValuesHolder.ofFloat("scaleY", 1.1f));
        scaleDown.setDuration(310);

        scaleDown.setRepeatCount(1);
        scaleDown.setRepeatMode(ObjectAnimator.REVERSE);

        scaleDown.start();

    }

    public void gotoMenu(View view) {
        Database.setDataofStopAction( new Date(  ) );
        Intent intent = new Intent( this, resume.class );
        startActivity( intent );
    }

    public void breath(View view) {
        Database.breath_counter++;
        breath.setText( Integer.toString( Database.breath_counter%2 ) );

        if(Database.breath_counter%2==0){
            breath_icon.setVisibility( View.INVISIBLE );
            breath.setVisibility( View.INVISIBLE );
            count=0;
        }
    }

    private class HeartRateSubscriptionTask extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... params) {
            try {
                if (getConnectedBandClient()) {
                    if (client.getSensorManager().getCurrentHeartRateConsent() == UserConsent.GRANTED) {
                        client.getSensorManager().registerHeartRateEventListener(mHeartRateEventListener);
                    } else {
         //               appendToUI("You have not given this application consent to access heart rate data yet."
         //                       + " Please press the Heart Rate Consent button.\n");
                    }
                } else {
           //         appendToUI("Band isn't connected. Please make sure bluetooth is on and the band is in range.\n");
                }
            } catch (BandException e) {
                String exceptionMessage="";
                switch (e.getErrorType()) {
                    case UNSUPPORTED_SDK_VERSION_ERROR:
                        exceptionMessage = "Microsoft Health BandService doesn't support your SDK Version. Please update to latest SDK.\n";
                        break;
                    case SERVICE_ERROR:
                        exceptionMessage = "Microsoft Health BandService is not available. Please make sure Microsoft Health is installed and that you have the correct permissions.\n";
                        break;
                    default:
                        exceptionMessage = "Unknown error occured: " + e.getMessage() + "\n";
                        break;
                }
         //       appendToUI(exceptionMessage);

            } catch (Exception e) {
        //        appendToUI(e.getMessage());
            }
            return null;
        }
    }

    private class HeartRateConsentTask extends AsyncTask<WeakReference<Activity>, Void, Void> {
        @Override
        protected Void doInBackground(WeakReference<Activity>... params) {
            try {
                if (getConnectedBandClient()) {

                    if (params[0].get() != null) {
                        client.getSensorManager().requestHeartRateConsent(params[0].get(), new HeartRateConsentListener() {
                            @Override
                            public void userAccepted(boolean consentGiven) {
                            }
                        });
                    }
                } else {
              //      appendToUI("Band isn't connected. Please make sure bluetooth is on and the band is in range.\n");
                }
            } catch (BandException e) {
                String exceptionMessage="";
                switch (e.getErrorType()) {
                    case UNSUPPORTED_SDK_VERSION_ERROR:
                        exceptionMessage = "Microsoft Health BandService doesn't support your SDK Version. Please update to latest SDK.\n";
                        break;
                    case SERVICE_ERROR:
                        exceptionMessage = "Microsoft Health BandService is not available. Please make sure Microsoft Health is installed and that you have the correct permissions.\n";
                        break;
                    default:
                        exceptionMessage = "Unknown error occured: " + e.getMessage() + "\n";
                        break;
                }
             //   appendToUI(exceptionMessage);

            } catch (Exception e) {
          //      appendToUI(e.getMessage());
            }
            return null;
        }
    }

    private void appendToUI(final String string) {
        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                //txtStatus.setText(string);
            }
        });
    }

    private boolean getConnectedBandClient() throws InterruptedException, BandException {
        if (client == null) {
            BandInfo[] devices = BandClientManager.getInstance().getPairedBands();
            if (devices.length == 0) {
             //   appendToUI("Band isn't paired with your phone.\n");
                return false;
            }
            client = BandClientManager.getInstance().create(getBaseContext(), devices[0]);
        } else if (ConnectionState.CONNECTED == client.getConnectionState()) {
            return true;
        }

       // appendToUI("Band is connecting...\n");
        return ConnectionState.CONNECTED == client.connect().await();
    }


    public void onClickMap2(View view) {
        ImageView mapView = findViewById( R.id.mapa );
        if (mapView.getVisibility() == View.VISIBLE) {
            mapView.setVisibility( View.INVISIBLE );
        } else {
            mapView.setVisibility( View.VISIBLE );
        }
    }

    int last=0;
    @Override
    public void onSensorChanged(SensorEvent event) {

        last=last+1;

        float recent=0;
        Log.d("Acc", Float.toString((event.values[0]) ));
        Log.d("Acc1", Float.toString((event.values[1]) ));
        Log.d("Acc2", Float.toString((event.values[2]) ));
        if(event.values[0]>9 ||event.values[1]>9 ||event.values[0]<-9 ||event.values[1]<-9  ) {}
        else{
            if (dataAccelerometr.size() < 100) {
                dataAccelerometr.add( event.values[2] );
            } else {
                dataAccelerometr.remove( 0 );
                dataAccelerometr.add( event.values[2] );
            }
        }
        if(dataAccelerometr.size()>2) {
            if (dataAccelerometr.get( dataAccelerometr.size() - 2 ) > 12.5 && last > 5) {
                if (dataAccelerometr.get( dataAccelerometr.size() - 1 ) < dataAccelerometr.get( dataAccelerometr.size() - 2 )) {
             //      if(dataAccelerometr.get( dataAccelerometr.size()-1)-dataAccelerometr.get( dataAccelerometr.size()-2 )>6 || dataAccelerometr.get( dataAccelerometr.size()-1)-dataAccelerometr.get( dataAccelerometr.size()-2 )<-6){

             //      }else
                    {
                       count++;
                       last = 0;
                       Log.d( "Acc", "++++++++++++++++++++" );
                       ObjectAnimator scaleDown = ObjectAnimator.ofPropertyValuesHolder(
                               aim,
                               PropertyValuesHolder.ofFloat( "scaleX", 1.4f ),
                               PropertyValuesHolder.ofFloat( "scaleY", 1.4f ) );
                       scaleDown.setDuration( 50 );

                       scaleDown.setRepeatCount( 1 );
                       scaleDown.setRepeatMode( ObjectAnimator.REVERSE );

                       scaleDown.start();

                       ObjectAnimator scaleDown2 = ObjectAnimator.ofPropertyValuesHolder(
                               breath,
                               PropertyValuesHolder.ofFloat( "scaleX", 1.4f ),
                               PropertyValuesHolder.ofFloat( "scaleY", 1.4f ) );
                       scaleDown.setDuration( 50 );

                       scaleDown.setRepeatCount( 1 );
                       scaleDown.setRepeatMode( ObjectAnimator.REVERSE );
                       ObjectAnimator scaleDown3 = ObjectAnimator.ofPropertyValuesHolder(
                               breath_icon,
                               PropertyValuesHolder.ofFloat( "scaleX", 1.4f ),
                               PropertyValuesHolder.ofFloat( "scaleY", 1.4f ) );
                       scaleDown.setDuration( 50 );

                       scaleDown.setRepeatCount( 1 );
                       scaleDown.setRepeatMode( ObjectAnimator.REVERSE );

                       scaleDown3.start();
                       scaleDown2.start();
                   }


                    if(count<=30) {
                        count_down.setText( Integer.toString( 30 - count ) );
                        count_to.setText( Integer.toString( count ) );
                    }
                    if(count==30){
                            breath_icon.setVisibility( View.VISIBLE );
                            breath.setVisibility( View.VISIBLE );
                            Database.cycle++;
                    }

                    Database.push_counter++;

                }
            }
        }



        switch (event.sensor.getType())
        {
            case Sensor.TYPE_MAGNETIC_FIELD:
                mags = event.values.clone();
                break;
            case Sensor.TYPE_ACCELEROMETER:
                accels = event.values.clone();
                break;
        }

        if (mags != null && accels != null) {
            Rot = new float[9];
            I= new float[9];
            SensorManager.getRotationMatrix(Rot, I, accels, mags);

            float[] outR = new float[9];
            SensorManager.remapCoordinateSystem(Rot, SensorManager.AXIS_X,SensorManager.AXIS_Z, outR);
            SensorManager.getOrientation(outR, values);

            azimuth = values[0] * 57.2957795f;
            pitch =values[1] * 57.2957795f;
            roll = values[2] * 57.2957795f;
            azymut.setText( Float.toString( azimuth ) );
            roll_t.setText(Float.toString( roll ));
            mags = null;
            accels = null;
        }
    }





    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }


    @Override
    public void onMapReady(GoogleMap map) {
        if (ActivityCompat.checkSelfPermission( this, Manifest.permission.ACCESS_FINE_LOCATION ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission( this, Manifest.permission.ACCESS_COARSE_LOCATION ) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        locationManager.requestLocationUpdates( "gps", 5000, 0, listener );
        Location location=null;
   //     while (location==null)
        location = locationManager.getLastKnownLocation( LocationManager.GPS_PROVIDER );

        if(location!=null) {
            mapa=1;
            gmap=map;
            NumberFormat formatter = new DecimalFormat( "#0.00" );
            lat.setText( formatter.format( location.getLatitude() ) );
            longLat.setText( formatter.format( location.getLongitude() ) );
            map.addMarker( new MarkerOptions().position( new LatLng( location.getLatitude(), location.getLongitude() ) ).title( "Marker" ) );
            float zoomLevel = 19.0f; //This goes up to 21
            map.moveCamera( CameraUpdateFactory.newLatLngZoom( new LatLng( location.getLatitude(), location.getLongitude() ), zoomLevel ) );
            map.setMapType( GoogleMap.MAP_TYPE_HYBRID );
        }
    }
}
