package com.ostrowski.healthpointedu;

import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.NumberPicker;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.microsoft.band.BandClient;
import com.microsoft.band.BandClientManager;
import com.microsoft.band.BandException;
import com.microsoft.band.BandInfo;
import com.microsoft.band.ConnectionState;
import com.microsoft.band.UserConsent;
import com.microsoft.band.sensors.BandGsrEvent;
import com.microsoft.band.sensors.BandGsrEventListener;
import com.microsoft.band.sensors.BandHeartRateEvent;
import com.microsoft.band.sensors.BandHeartRateEventListener;
import com.microsoft.band.sensors.HeartRateConsentListener;

import java.lang.ref.WeakReference;
import java.util.Random;

import static java.lang.Thread.sleep;

public class TriageMenu extends AppCompatActivity {

    ImageView heart_image;
    Handler handler = new Handler(  );
    Switch oddech, krew, transfuzja, hipotermia, wstrzas, okouraz, poparzenia;
    NumberPicker np, np2, np3, np4;
    private BandClient client = null;
    TextView pulse;
    private Random r = new Random();
    private BandHeartRateEventListener mHeartRateEventListener = new BandHeartRateEventListener() {
        @Override
        public void onBandHeartRateChanged(final BandHeartRateEvent event) {
            if (event != null) {
                Database.setHeart( event.getHeartRate() );
                appendToUI(Integer.toString( event.getHeartRate()));
            }
        }
    };



    private BandGsrEventListener mGsrEventListener = new BandGsrEventListener() {
        @Override
        public void onBandGsrChanged(final BandGsrEvent event) {
            if (event != null) {
                appendToUI2(Integer.toString( event.getResistance()));
            }
        }
    };

    private class GsrSubscriptionTask extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... params) {
            try {
                if (getConnectedBandClient()) {
                    int hardwareVersion = Integer.parseInt(client.getHardwareVersion().await());
                    if (hardwareVersion >= 20) {
                       // appendToUI("Band is connected.\n");
                        client.getSensorManager().registerGsrEventListener(mGsrEventListener);
                    } else {
               //         appendToUI("The Gsr sensor is not supported with your Band version. Microsoft Band 2 is required.\n");
                    }
                } else {
              //      appendToUI("Band isn't connected. Please make sure bluetooth is on and the band is in range.\n");
                }
            } catch (BandException e) {
                String exceptionMessage="";
                switch (e.getErrorType()) {
                    case UNSUPPORTED_SDK_VERSION_ERROR:
                        exceptionMessage = "Microsoft Health BandService doesn't support your SDK Version. Please update to latest SDK.\n";
                        break;
                    case SERVICE_ERROR:
                        exceptionMessage = "Microsoft Health BandService is not available. Please make sure Microsoft Health is installed and that you have the correct permissions.\n";
                        break;
                    default:
                        exceptionMessage = "Unknown error occured: " + e.getMessage() + "\n";
                        break;
                }
           //     appendToUI(exceptionMessage);

            } catch (Exception e) {
           //     appendToUI(e.getMessage());
            }
            return null;
        }
    }

    private void appendToUI2(final String string) {
        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
           //     gsr.setText(string);
            }
        });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_triage_menu );

        oddech = findViewById( R.id.oddech );
        krew = findViewById( R.id.krwawienie );
        transfuzja = findViewById( R.id.transfuzja );
        hipotermia = findViewById( R.id.hipotermia );
        wstrzas = findViewById( R.id.wstrzas );
        okouraz = findViewById( R.id.oko );
        poparzenia = findViewById( R.id.oparzenia );

        pulse=findViewById( R.id.puls );
   //     gsr=findViewById( R.id.gsr );



        heart_image = findViewById( R.id.heart );

        pulse.setText( Integer.toString( 70 ));

        new Thread( new Runnable() {
            @Override
            public void run() {
                while (true){
                    try {
                        sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    handler.post( new Runnable() {
                        @Override
                        public void run() {
                            pulse();
                        }
                    } );
                }
            }
        } ).start();

        final WeakReference<Activity> reference = new WeakReference<Activity>(this);
        new HeartRateSubscriptionTask().execute();
        new HeartRateConsentTask().execute(reference);
        new GsrSubscriptionTask().execute();
    }

    private class HeartRateSubscriptionTask extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... params) {
            try {
                if (getConnectedBandClient()) {
                    if (client.getSensorManager().getCurrentHeartRateConsent() == UserConsent.GRANTED) {
                        client.getSensorManager().registerHeartRateEventListener(mHeartRateEventListener);
                    } else {
               //         appendToUI("You have not given this application consent to access heart rate data yet."
                //                + " Please press the Heart Rate Consent button.\n");
                    }
                } else {
               //     appendToUI("Band isn't connected. Please make sure bluetooth is on and the band is in range.\n");
                }
            } catch (BandException e) {
                String exceptionMessage="";
                switch (e.getErrorType()) {
                    case UNSUPPORTED_SDK_VERSION_ERROR:
                        exceptionMessage = "Microsoft Health BandService doesn't support your SDK Version. Please update to latest SDK.\n";
                        break;
                    case SERVICE_ERROR:
                        exceptionMessage = "Microsoft Health BandService is not available. Please make sure Microsoft Health is installed and that you have the correct permissions.\n";
                        break;
                    default:
                        exceptionMessage = "Unknown error occured: " + e.getMessage() + "\n";
                        break;
                }
             //   appendToUI(exceptionMessage);

            } catch (Exception e) {
            //    appendToUI(e.getMessage());
            }
            return null;
        }
    }

    private class HeartRateConsentTask extends AsyncTask<WeakReference<Activity>, Void, Void> {
        @Override
        protected Void doInBackground(WeakReference<Activity>... params) {
            try {
                if (getConnectedBandClient()) {

                    if (params[0].get() != null) {
                        client.getSensorManager().requestHeartRateConsent(params[0].get(), new HeartRateConsentListener() {
                            @Override
                            public void userAccepted(boolean consentGiven) {
                            }
                        });
                    }
                } else {
                //    appendToUI("Band isn't connected. Please make sure bluetooth is on and the band is in range.\n");
                }
            } catch (BandException e) {
                String exceptionMessage="";
                switch (e.getErrorType()) {
                    case UNSUPPORTED_SDK_VERSION_ERROR:
                        exceptionMessage = "Microsoft Health BandService doesn't support your SDK Version. Please update to latest SDK.\n";
                        break;
                    case SERVICE_ERROR:
                        exceptionMessage = "Microsoft Health BandService is not available. Please make sure Microsoft Health is installed and that you have the correct permissions.\n";
                        break;
                    default:
                        exceptionMessage = "Unknown error occured: " + e.getMessage() + "\n";
                        break;
                }
           //     appendToUI(exceptionMessage);

            } catch (Exception e) {
           //     appendToUI(e.getMessage());
            }
            return null;
        }
    }

    private void appendToUI(final String string) {
        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
               pulse.setText(string);
            }
        });
    }

    private boolean getConnectedBandClient() throws InterruptedException, BandException {
        if (client == null) {
            BandInfo[] devices = BandClientManager.getInstance().getPairedBands();
            if (devices.length == 0) {
                appendToUI("Band isn't paired with your phone.\n");
                return false;
            }
            client = BandClientManager.getInstance().create(getBaseContext(), devices[0]);
        } else if (ConnectionState.CONNECTED == client.getConnectionState()) {
            return true;
        }

     //   appendToUI("Band is connecting...\n");
        return ConnectionState.CONNECTED == client.connect().await();
    }



    private void pulse() {
        ObjectAnimator scaleDown = ObjectAnimator.ofPropertyValuesHolder(
                heart_image,
                PropertyValuesHolder.ofFloat("scaleX", 1.1f),
                PropertyValuesHolder.ofFloat("scaleY", 1.1f));
        scaleDown.setDuration(410);

        scaleDown.setRepeatCount(1);
        scaleDown.setRepeatMode(ObjectAnimator.REVERSE);
        Random random = new Random(  );
        scaleDown.start();
    }

    public void onStart(View view) {


        int avpu = 12;
        if(oddech.isChecked()){
            Database.instrukcja[0]=1;
        }
        else{
            Database.instrukcja[0]=0;
        }

        if(krew.isChecked()){
            Database.instrukcja[1]=1;
        }
        else{
            Database.instrukcja[1]=0;
        }

        if(transfuzja.isChecked()){
            Database.instrukcja[2]=1;
        }
        else{
            Database.instrukcja[2]=0;
        }

        if(hipotermia.isChecked()){
            Database.instrukcja[3]=1;
        }
        else{
            Database.instrukcja[3]=0;
        }

        if(wstrzas.isChecked()){
            Database.instrukcja[4]=1;
        }
        else{
            Database.instrukcja[4]=0;
        }

        if(okouraz.isChecked()){
            Database.instrukcja[5]=1;
        }
        else{
            Database.instrukcja[5]=0;
        }

        if(poparzenia.isChecked()){
            Database.instrukcja[6]=1;
        }
        else{
            Database.instrukcja[6]=0;
        }

        if(avpu<5){
            // uszkodzenie mózgu
            Database.avpu=avpu;
            Toast.makeText(this, "Mózg został twale uszkodzony, akcja przerwana",
                    Toast.LENGTH_LONG).show();
            Intent intent = new Intent( this, resumetriage.class );
            startActivity( intent );

        }else{
            // instrukcja

            Database.instrukcja_strona=0;
            Toast.makeText(this, "Rozpoczynamy akcję ratunkową",
                    Toast.LENGTH_LONG).show();
            Database.avpu=avpu;
            Intent intent = new Intent( this, triage_instruction.class );
            startActivity( intent );
        }



    }
}
