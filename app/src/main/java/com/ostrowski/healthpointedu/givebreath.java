package com.ostrowski.healthpointedu;

import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.Date;

public class givebreath extends AppCompatActivity {

    int clock=0;
    Handler handler = new Handler(  );
    private TextView counter;
    private Context T;
    private MediaPlayer sound;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_givebreath );
        counter=findViewById( R.id.counter );
        T=this;
        sound = MediaPlayer.create( this, R.raw.give);
        sound.start();
        Database.setBreath_counter( Database.getBreath_counter()+2 );
        manager();
    }

    @Override
    public void onBackPressed() {
        moveTaskToBack(false);
    }

    public void manager(){
        Runnable start_clock=new Runnable() {
            @Override
            public void run() {
                while(true)
                    set();
            }
        };
        new Thread(start_clock).start();
    }

    private void set() {
        try {
            Thread.sleep( 1000 );
            clock++;

            handler.post( new Runnable() {
                @Override
                public void run() {
                    if(clock==5){
                        counter.setText( "1" );
                    }

                    if(clock==9){
                        counter.setText( "0" );
                    }

                    if(clock==10){
                        Intent intent = new Intent( T, Instruction.class );
                        startActivity( intent );
                    }
                }
            } );
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void stop(View view) {
        Database.setDataofStopAction( new Date(  ) );
        Intent intent = new Intent( this, resume.class );
        startActivity( intent );
    }
}

