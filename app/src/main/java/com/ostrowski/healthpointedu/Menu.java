package com.ostrowski.healthpointedu;

import android.Manifest;
import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

public class Menu extends AppCompatActivity {

    private ImageView game;
    private ImageView info;
    private ImageView history;
    private Context context;
    private ImageView icon;
    private TextView what;
    private TextView what_about;
    private ViewPagerAdapter viewPagerAdapter;
    private ViewPager viewPager;
    private RelativeLayout linearLayout, dotslayout;

    private Handler handler = new Handler(  );
    public int[] image={
            R.drawable.phone_icon,
            R.drawable.blood_icon,
            R.drawable.alergie_icon,
            R.drawable.medi_icon,
            R.drawable.disease
    };

    public String[] person={
            "26-12-2018",
            "56:34",
            "Truskawki",
            "Apap",
            "Cukrzyca"
    };

    public String[] info_what = {
            "Ostatnia akcja",
            "Czas trwania",
            "Alergie",
            "Przyjmowane leki",
            "Choroby"
    };


    int counter =0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_menu );

        viewPager=findViewById(R.id.view);
        linearLayout=findViewById(R.id.layout);
        viewPagerAdapter = new ViewPagerAdapter(this);
        viewPager.setAdapter(viewPagerAdapter);

        context=this;


        manager();
    }

    public void onClickGame(View view) {
        final Intent intent = new Intent(this, MainActivity.class);
        startActivity( intent );
    }

    public void onClickHistory(View view) {
        final Intent intent = new Intent(this, MainActivity.class);
        startActivity( intent );
    }

    public void manager() {
        Runnable start_clock = new Runnable() {
            @Override
            public void run() {
                while (true)
                    set();
            }
        };
        new Thread( start_clock ).start();
    }

    private void set() {
        try {
            Thread.sleep( 2000 );
            counter++;
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        handler.post( new Runnable() {
            @Override
            public void run() {
                ImageView iv;
                Random random = new Random(  );
                int los = random.nextInt()%3;

                iv = (ImageView)findViewById( R.id.game ) ;
              TextView it = (TextView) findViewById( R.id.gameText);

                ObjectAnimator scaleDown = ObjectAnimator.ofPropertyValuesHolder(
                        iv,
                        PropertyValuesHolder.ofFloat("scaleX", 1.1f),
                        PropertyValuesHolder.ofFloat("scaleY", 1.1f));
                scaleDown.setDuration(310);

                scaleDown.setRepeatCount(1);
                scaleDown.setRepeatMode(ObjectAnimator.REVERSE);

                scaleDown.start();

                ObjectAnimator scaleDown2 = ObjectAnimator.ofPropertyValuesHolder(
                        it,
                        PropertyValuesHolder.ofFloat("scaleX", 1.1f),
                        PropertyValuesHolder.ofFloat("scaleY", 1.1f));
                scaleDown2.setDuration(310);

                scaleDown2.setRepeatCount(1);
                scaleDown2.setRepeatMode(ObjectAnimator.REVERSE);

                scaleDown2.start();





            }
        } );
    }

    public static void ImageViewAnimatedChange(Context c, final ImageView v, final int new_image) {
        final Animation anim_out = AnimationUtils.loadAnimation( c, android.R.anim.fade_out );
        final Animation anim_in = AnimationUtils.loadAnimation( c, android.R.anim.fade_in );
        anim_out.setAnimationListener( new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                v.setImageResource( new_image );
                anim_in.setAnimationListener( new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {
                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                    }
                } );
                v.startAnimation( anim_in );
            }
        } );
        v.startAnimation( anim_out );
    }

    public static void TextViewAnimatedChange(Context c, final TextView v, final String new_text) {
        final Animation anim_out = AnimationUtils.loadAnimation( c, android.R.anim.fade_out );
        final Animation anim_in = AnimationUtils.loadAnimation( c, android.R.anim.fade_in );
        anim_out.setAnimationListener( new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                v.setText( new_text );
                anim_in.setAnimationListener( new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {
                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                    }
                } );
                v.startAnimation( anim_in );
            }
        } );
        v.startAnimation( anim_out );
    }

    public void onClickInfo(View view) {
        Database.cycle=0;
        Database.push_counter=0;
        Database.breath_counter=0;
        Intent intent = new Intent( this, Instruction.class );
        startActivity( intent );
    }

    public void gototriage(View view) {
        Database.setDataOfStartAction( new Date(  ) );
        Intent intent = new Intent( this, TriageMenu.class );
        startActivity( intent );
    }

    public void gotoSMS(View view) {

        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.CALL_PHONE)
                != PackageManager.PERMISSION_GRANTED) {

            // Permission is not granted
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.CALL_PHONE)) {
                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
            } else {
                // No explanation needed; request the permission
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.CALL_PHONE},
                        12);

                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        }

        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.SEND_SMS)
                != PackageManager.PERMISSION_GRANTED) {

            // Permission is not granted
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.SEND_SMS)) {
                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
            } else {
                // No explanation needed; request the permission
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.SEND_SMS},
                        13);

                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        }

        SmsManager smsManager = SmsManager.getDefault();
        smsManager.sendTextMessage("+48724692649", null, "Proszę o pomoc, moja lokalizacja to: 52.106035 , 20.8224256, 05-830 Nadarzyn ", null, null);


        Intent callIntent =new Intent(Intent.ACTION_CALL);
        callIntent.setData(Uri.parse("tel:112"));
        startActivity(callIntent);





    }

    public void ice(View view) {
        Intent intent = new Intent(  this, ICE.class);
        startActivity( intent );

    }
}
