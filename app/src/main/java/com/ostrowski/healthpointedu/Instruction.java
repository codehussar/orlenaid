package com.ostrowski.healthpointedu;

import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.content.Context;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.media.MediaPlayer;
import android.os.Handler;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static java.lang.Thread.sleep;

public class Instruction extends AppCompatActivity {

    private Handler handler = new Handler(  );
    private TextView instruction;
    private ImageView checkbreath;
    private ImageView hex;
    private ImageView picture;
    private TextView cn;
    private int counter = 0;
    private int clock=0;
    private List<Float> dataAccelerometr=new ArrayList<Float>();
    private int count=0;
    private Animation animation;
    private Animation animation_out;
    private MediaPlayer sound;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_instruction );
        sound = MediaPlayer.create( this, R.raw.checkbreath);
        sound.start();
        if(Database.getDataOfStartAction()==null)
          Database.setDataOfStartAction( new Date( ));

    }

    @Override
    public void onBackPressed() {
        moveTaskToBack(false);
    }

    public void onClickNo(View view) {
        Database.setCycle( Database.getCycle()+1 );
        Intent intent = new Intent( this, CPR.class );
        startActivity( intent );
    }

    public void onClickyes(View view) {
        Database.setDataofStopAction( new Date(  ) );
        Intent intent = new Intent( this, resume.class );
        startActivity( intent );
    }
}

