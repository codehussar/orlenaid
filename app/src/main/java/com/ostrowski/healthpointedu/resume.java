package com.ostrowski.healthpointedu;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.text.SimpleDateFormat;
import java.util.Date;

public class resume extends AppCompatActivity {

    @Override
    public void onBackPressed() {
        moveTaskToBack(false);
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_resume );

        Toast.makeText(this, "Akcja zakończona !!! Pobieranie statystyk",
                Toast.LENGTH_LONG).show();

        Animation animation = AnimationUtils.loadAnimation(this,R.anim.slide_right);
        Animation animation2 = AnimationUtils.loadAnimation(this,R.anim.slide_right);
        Animation animation3 = AnimationUtils.loadAnimation(this,R.anim.slide_right);
        animation2.setStartOffset( 500 );
        animation3.setStartOffset( 1000 );
        //start Background
        ImageView start_back = findViewById( R.id.start_back );
        start_back.startAnimation( animation );

        // Text start time

        SimpleDateFormat sdf = new SimpleDateFormat( "HH:mm:ss" );
        String currentDateandTime = sdf.format( Database.getDataOfStartAction());
        TextView startTime = findViewById( R.id.timeStart );
        startTime.setText( currentDateandTime );
        startTime.startAnimation( animation );


        //stop Background
        ImageView stop_back = findViewById( R.id.stop_back );
        stop_back.startAnimation( animation2 );

        // Text stop time

        currentDateandTime = sdf.format( Database.getDataofStopAction());
        TextView stopTime = findViewById( R.id.stoptime );
        stopTime.setText( currentDateandTime );
        stopTime.startAnimation( animation2);

        //Duration Background
        ImageView duration_back = findViewById( R.id.duration_back );
        duration_back.startAnimation( animation3 );

        // Text Duration time
        TextView duration = findViewById( R.id.time );

        Date t1 = Database.getDataofStopAction();
        Date t2 = Database.getDataOfStartAction();
        long diff = t1.getTime() - t2.getTime();
        long diffSeconds = (diff / 1000) % 60;
        String sec, min, hrs;

        if (diffSeconds < 10) {
            sec = "0" + Long.toString( diffSeconds );
        } else {
            sec = Long.toString( diffSeconds );
        }

        long diffMinutes = (diff / (60 * 1000)) % 60;

        if (diffMinutes < 10) {
            min = "0" + Long.toString( diffMinutes );
        } else {
            min = Long.toString( diffMinutes );
        }

        long diffHours = diff / (60 * 60 * 1000);

        if (diffHours < 10) {
            hrs = "0" + Long.toString( diffHours );
        } else {
            hrs = Long.toString( diffHours );
        }
        String durationText= hrs + ":" + min + ":" + sec;
        duration.setText( durationText );

        duration.startAnimation( animation3 );


        // From left animation

        Animation animation4 = AnimationUtils.loadAnimation(this,R.anim.slide_left);
        Animation animation5 = AnimationUtils.loadAnimation(this,R.anim.slide_left);
        Animation animation6 = AnimationUtils.loadAnimation(this,R.anim.slide_left);
        animation5.setStartOffset( 500 );
        animation6.setStartOffset( 1000 );

        //cycle Background
        ImageView cycle_back = findViewById( R.id.cycle_back);
        cycle_back.startAnimation( animation4 );

        // Text cycle time

        TextView cycle = findViewById( R.id.cycle );
        int cycleNumber=Database.getCycle();
        cycle.setText(  Integer.toString( cycleNumber ));
        cycle.startAnimation( animation4 );

        //breath Background
        ImageView breath_back = findViewById( R.id.breath_back );
        breath_back.startAnimation( animation5 );

        // Text breath time

        TextView breath = findViewById( R.id.breath );
        int breathNumber=Database.getBreath_counter();
        breath.setText( Integer.toString( breathNumber ) );
        breath.startAnimation( animation5 );


        //push Background
        ImageView push_back = findViewById( R.id.push_back );
        push_back.startAnimation( animation6 );

        // Text push time

        TextView push = findViewById( R.id.push);
        int pushNumber=Database.getPush_counter();
        push.setText(  Integer.toString( pushNumber ));
        push.startAnimation( animation6 );


    }

    public void gotoMenu(View view) {
        Intent intent = new Intent( this, Menu.class );
        startActivity( intent );
    }
}
