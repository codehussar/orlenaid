package com.ostrowski.healthpointedu;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

public class Splash_Activity extends AppCompatActivity {

    private ImageView imageView;
    private Context object = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_);
        Animation animation = AnimationUtils.loadAnimation(this,R.anim.splash_screen);
        Animation animation_out = AnimationUtils.loadAnimation(object, R.anim.splash_screen_out);
        final Intent intent = new Intent(this, Menu.class);

        imageView = (ImageView) findViewById(R.id.logo);
        imageView.startAnimation(animation);
        Thread timer = new Thread(){
            @Override
            public void run() {
                try {
                    sleep(8000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }finally {
                    startActivity( intent);
                    finish();
                }
            }
        };
        timer.start();
        //imageView.startAnimation(animation_out);

    }


}
