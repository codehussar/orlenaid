package com.ostrowski.healthpointedu;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class ViewPagerAdapter extends PagerAdapter {

    private Context context;
    private LayoutInflater layoutInflater;
    private Integer integer;

    public ViewPagerAdapter(Context context) {
        this.context = context;
    }

    public String slide_okno[] = {
            "pierwsze",
            "drugie"
    };

    public int[] image={
            R.drawable.phone_icon,
            R.drawable.clo,
            R.drawable.gps,
            R.drawable.medi_icon,
            R.drawable.disease
    };

    public String[] person={
            "Al. Katowicka 62,",
            "Nadarzyn",
            "112",
            "Apap",
            "Cukrzyca"
    };

    public String[] info_what = {
            "Location",
            "Location",
            "Numer alarmowy",
            "Przyjmowane leki",
            "Choroby"
    };


    @Override
    public int getCount() {
        return 3;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == (RelativeLayout) object;
    }

    public Object instantiateItem(ViewGroup container, int position){
        layoutInflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);

        View view = layoutInflater.inflate(R.layout.slide_layout, container, false);


        TextView pole3= (TextView) view.findViewById(R.id.dane );

        TextView tytul1= (TextView) view.findViewById(R.id.opis );

        ImageView ikonka1 = (ImageView)  view.findViewById(R.id.ikona);



        pole3.setText(person[position%5]);

        ikonka1.setImageResource(image[position%5]);

        tytul1.setText(info_what[position%5]);


        container.addView(view);
        return view;
    }

    public void destroyItem( ViewGroup container, int position, Object object){

        container.removeView((RelativeLayout)object);
    }
}
