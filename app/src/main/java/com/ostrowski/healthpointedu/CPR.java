package com.ostrowski.healthpointedu;

import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class CPR extends AppCompatActivity implements SensorEventListener {
    private List<Float> dataAccelerometr=new ArrayList<Float>();
    private int count=0;
    private TextView counter;
    private MediaPlayer sound;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_cpr );
        counter=findViewById( R.id.counter );
        SensorManager sensorManager = (SensorManager) getSystemService( SENSOR_SERVICE );
        sensorManager.registerListener( this, sensorManager.getDefaultSensor( Sensor.TYPE_ACCELEROMETER ), SensorManager.SENSOR_DELAY_NORMAL );
        sound = MediaPlayer.create( this, R.raw.cpr);
        sound.start();
    }

    @Override
    public void onBackPressed() {
        moveTaskToBack(false);
    }

    int last=0;
    @Override
    public void onSensorChanged(SensorEvent event) {
        last=last+1;


        Log.d("Acc", Float.toString((event.values[2]) ));
        if(dataAccelerometr.size()<100){
            dataAccelerometr.add(event.values[2]);
        }else{
            dataAccelerometr.remove( 0 );
            dataAccelerometr.add( event.values[2] );
        }
        if(dataAccelerometr.size()>2) {
            if (dataAccelerometr.get( dataAccelerometr.size() - 2 ) > 12 && last > 5) {
                if (dataAccelerometr.get( dataAccelerometr.size() - 1 ) < dataAccelerometr.get( dataAccelerometr.size() - 2 )) {
                    count++;
                    Database.setPush_counter( Database.getPush_counter()+1 );
                    last = 0;
                    Log.d( "Acc", "++++++++++++++++++++" );
                    counter.setText( Integer.toString( 30- count ) );
                    if(count==30){
                        Intent intent = new Intent( this, givebreath.class );
                        startActivity(intent);
                    }
                }
            }
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    public void stop(View view) {
        Database.setDataofStopAction( new Date(  ) );
        Intent intent = new Intent( this, resume.class );
        startActivity( intent );
    }
}
